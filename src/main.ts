import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { join } from 'path';
import { NestExpressApplication } from '@nestjs/platform-express';
import expressLayouts = require('express-ejs-layouts');
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);

  app.setBaseViewsDir(join(__dirname, '..', '..', 'views'));
  app.setViewEngine('ejs');
  app.set('layout extractScripts', true);
  app.set('layout', 'layout/index.ejs');
  app.use(expressLayouts);

  app.use(function (req, res, next) {
    const url = req.originalUrl;
    const unauthedUrls = ['/registration', '/login'];
    if (url.match('view')) {
      if (!unauthedUrls.some((el) => url.match(el))) {
        app.set('layout', 'layout/authRequired');
      }
    }
    return next();
  });
  const config = new DocumentBuilder()
    .setTitle('Nest Starter')
    .setDescription('You are welcome!')
    .setVersion('1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('swagger', app, document);
  await app.listen(3000);
}
bootstrap();
