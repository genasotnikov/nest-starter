import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { User as IUser } from './user.interface';

@Entity('users')
export class User implements IUser {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  username: string;

  @Column({ nullable: false })
  password: string;
}
