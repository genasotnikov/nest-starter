import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { DatabaseModule } from '../database.module';
import { ViewController } from './view/view.controller';
import { usersRepository } from './users-repository';

@Module({
  imports: [DatabaseModule],
  exports: [UsersService],
  controllers: [UsersController, ViewController],
  providers: [usersRepository, UsersService],
})
export class UsersModule {}
