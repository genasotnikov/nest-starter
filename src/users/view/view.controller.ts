import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Redirect,
  Render,
} from '@nestjs/common';
import { CreateUserDto } from '../dto/create-user.dto';
import { UsersService } from '../users.service';
import { UpdateUserDto } from '../dto/update-user.dto';
import { ApiExcludeEndpoint, ApiTags } from '@nestjs/swagger';

@Controller('view/users')
export class ViewController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  @Redirect('users')
  @ApiExcludeEndpoint()
  async createUser(@Body() createUserDto: CreateUserDto) {
    await this.usersService.create(createUserDto);
  }

  @Get()
  @Render('users')
  @ApiExcludeEndpoint()
  async getUsers() {
    const users = await this.usersService.findAll();
    return { users };
  }

  @Post('/:id')
  @Redirect('/view/users')
  @ApiExcludeEndpoint()
  async editUser(@Param() { id }, @Body() newUserData: UpdateUserDto) {
    await this.usersService.update(+id, newUserData);
  }

  @Get('/delete/:id')
  @Redirect('/view/users')
  @ApiExcludeEndpoint()
  async deleteUser(@Param() { id }) {
    await this.usersService.remove(+id);
  }

  @Get('/:id')
  @Render('users/userForm')
  @ApiExcludeEndpoint()
  async editUserPage(@Param() { id }) {
    const user = await this.usersService.findOne(+id);
    return { user, title: 'Edit User', buttonText: 'Save', action: '' };
  }
}
