import { Inject, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { PROVIDERS } from '../constants';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import { ObjectLiteral } from 'typeorm/common/ObjectLiteral';

@Injectable()
export class UsersService {
  constructor(
    @Inject(PROVIDERS.USER_REPOSITORY)
    private userRep: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const res = await this.userRep.create(createUserDto);
    await this.userRep.save(res);
    return 'Success';
  }

  findAll() {
    return this.userRep.find();
  }

  findOne(id: number) {
    return this.userRep.findOne({ where: { id } });
  }

  find(param: FindConditions<User>[] | FindConditions<User> | ObjectLiteral) {
    return this.userRep.find({ where: param });
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return this.userRep.update(id, updateUserDto);
  }

  remove(id: number) {
    return this.userRep.delete(id);
  }
}
