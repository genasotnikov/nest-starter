import { PROVIDERS } from '../constants';
import { Connection } from 'typeorm';
import { User } from './entities/user.entity';

export const usersRepository = {
  provide: PROVIDERS.USER_REPOSITORY,
  useFactory: (connection: Connection) => {
    try {
      const conn = connection.getRepository(User);
      return conn;
    } catch (e) {
      console.error(e);
    }
  },
  inject: [PROVIDERS.DATABASE_CONNECTION],
};
