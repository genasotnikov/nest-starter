import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { ApiBearerAuth, ApiBody, ApiHeader, ApiTags } from '@nestjs/swagger';
import { LoginDto } from './dto/login.dto';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  @ApiBody({ type: LoginDto })
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @Post('registration')
  async registration(@Body() body: CreateUserDto) {
    const res = await this.authService.registration(body);
    if (res === 'Validation Error') {
      throw new BadRequestException();
    }
    return 'User was successfully created';
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get('user-data')
  userData(@Request() req) {
    return req.user;
  }
}
