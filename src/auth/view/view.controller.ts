import { Body, Controller, Get, Post, Render, Res, UseGuards } from '@nestjs/common';
import { UsersService } from '../../users/users.service';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from '../auth.service';
import { CreateUserDto } from '../../users/dto/create-user.dto';
import { ApiExcludeEndpoint } from '@nestjs/swagger';

@Controller('view/auth')
export class ViewController {
  constructor(private authService: AuthService) {}

  @UseGuards(AuthGuard('local'))
  @Post()
  @ApiExcludeEndpoint()
  async login(@Res() response) {
    try {
      return response.redirect('/view/users');
    } catch (e) {}
  }

  @Get('login')
  @Render('login')
  @ApiExcludeEndpoint()
  getLoginPage() {
    console.log('User is on login page');
  }

  @Get('registration')
  @Render('users/registration')
  @ApiExcludeEndpoint()
  getRegistrationPage() {
    console.log('User is on registration page');
  }

  @Post()
  @ApiExcludeEndpoint()
  async registration(@Res() response, @Body() body: CreateUserDto) {
    await this.authService.registration(body);
    return response.redirect('login');
  }
}
