import { createConnection } from 'typeorm';
import { PROVIDERS } from './constants';

export const databaseProviders = [
  {
    provide: PROVIDERS.DATABASE_CONNECTION,
    useFactory: async () =>
      await createConnection({
        type: 'postgres',
        host: 'localhost',
        port: 5432,
        username: 'postgres',
        password: 'utyf8978260140',
        database: 'crud',
        migrations: [__dirname + '/../migrations'],
        entities: [__dirname + '/**/entities/*.entity{.ts,.js}'],
      }),
  },
];
