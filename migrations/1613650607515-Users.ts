import { MigrationInterface, QueryRunner, Table, TableColumn } from 'typeorm';

export class Users1613650607515 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const usersTable = new Table({
      name: 'users',
      columns: [
        new TableColumn({
          name: 'id',
          type: 'number',
          generationStrategy: 'increment',
          isPrimary: true,
          isGenerated: true,
          isNullable: true,
        }),
        new TableColumn({
          name: 'username',
          width: 50,
          type: 'varchar',
          isUnique: true,
          isNullable: false,
        }),
        new TableColumn({
          name: 'password',
          width: 100,
          type: 'varchar',
          isNullable: false,
        }),
      ],
    });
    await queryRunner.query(
      `CREATE TABLE "users" (
            "id" bigint, 
            "username" varchar NOT NULL, 
            "password" varchar NOT NULL, 
            CONSTRAINT "UQ_fe0bb3f6520ee0469504521e710" UNIQUE ("username"), 
            CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.query('DROP TABLE "users"');
  }
}
